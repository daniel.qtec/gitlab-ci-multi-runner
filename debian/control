Source: gitlab-ci-multi-runner
Section: devel
Priority: optional
Standards-Version: 4.5.0
Maintainer: Dmitry Smirnov <onlyjob@debian.org>
Uploaders: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Build-Depends: debhelper-compat (= 12) ,dh-golang
              ,golang-go (>= 2:1.7~)
    ,golang-github-azure-go-ansiterm-dev
    ,golang-github-azure-go-autorest-dev (>= 14.1.1~)
    ,golang-github-blang-semver-dev
    ,golang-github-bmatcuk-doublestar-dev
    ,golang-github-burntsushi-toml-dev
    ,golang-github-docker-docker-credential-helpers-dev (>= 0.5.0~)
    ,golang-github-docker-docker-dev (>= 17.12.1~)
#       ,golang-github-docker-distribution-dev
    ,golang-github-docker-go-connections-dev (>= 0.4.0~)
    ,golang-github-docker-go-units-dev
    ,golang-github-docker-spdystream-dev
    ,golang-github-dustin-go-humanize-dev
    ,golang-github-emicklei-go-restful-dev (>= 2.4.0~)
    ,golang-github-emicklei-go-restful-swagger12-dev
    ,golang-github-getsentry-raven-go-dev
    ,golang-github-ghodss-yaml-dev
    ,golang-github-gofrs-flock-dev
    ,golang-github-go-ini-ini-dev
    ,golang-github-googleapis-gnostic-dev
    ,golang-github-google-gofuzz-dev
    ,golang-github-gophercloud-gophercloud-dev
    ,golang-github-gorhill-cronexpr-dev
    ,golang-github-gorilla-context-dev
    ,golang-github-gorilla-mux-dev
    ,golang-github-hashicorp-go-version-dev
    ,golang-github-howeyc-gopass-dev
    ,golang-github-imdario-mergo-dev
    ,golang-github-jpillora-backoff-dev
    ,golang-github-json-iterator-go-dev
    ,golang-github-mattn-go-zglob-dev
    ,golang-github-minio-minio-go-dev (>= 6.0.11~)
    ,golang-github-pkg-errors-dev
    ,golang-github-pmezard-go-difflib-dev
    ,golang-github-prometheus-client-golang-dev (>= 0.9.0~)
    ,golang-github-sirupsen-logrus-dev (>= 1.0.6~)
    ,golang-github-spf13-pflag-dev
    ,golang-github-stretchr-testify-dev (>= 1.2.1~)
    ,golang-github-urfave-cli-dev
    ,golang-golang-x-crypto-dev
    ,golang-golang-x-oauth2-google-dev
    ,golang-golang-x-sync-dev
    ,golang-gopkg-inf.v0-dev
    ,golang-yaml.v2-dev
# tests:
    ,ca-certificates
    ,git
    ,golang-github-golang-mock-dev
    ,tzdata
Homepage: https://gitlab.com/gitlab-org/gitlab-runner
Vcs-Browser: https://salsa.debian.org/go-team/packages/gitlab-ci-multi-runner
Vcs-Git: https://salsa.debian.org/go-team/packages/gitlab-ci-multi-runner.git
XS-Go-Import-Path: gitlab.com/gitlab-org/gitlab-ci-multi-runner

Package: gitlab-runner
Provides: gitlab-ci-multi-runner
Conflicts: gitlab-ci-multi-runner
Breaks: gitlab-ci-multi-runner
Replaces: gitlab-ci-multi-runner
Architecture: any
Built-Using: ${misc:Built-Using}
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
    ,adduser
    ,ca-certificates
#    ,curl
    ,git
    ,lsb-base
Recommends: ${misc:Recommends}
    ,cdebootstrap
    ,git-lfs
    ,xz-utils
Suggests: ${misc:Suggests}
    ,docker.io
Description: GitLab Runner - runs continuous integration (CI) jobs
 GitLab Runner runs tests and sends the results to GitLab.
 GitLab Continuous Integration (CI) service (included with GitLab)
 coordinates the testing.
